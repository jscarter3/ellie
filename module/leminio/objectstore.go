package leminio

import (
	"bytes"
	"errors"
	"io/ioutil"
	"strings"

	minio "github.com/minio/minio-go"
	"gitlab.com/jscarter3/ellie"
)

type MinioObjectStore struct {
	client *minio.Client
	bucket string
}

func NewObjectStore(endpoint, bucket, accessKey, secretKey string) (*MinioObjectStore, error) {
	client, err := minio.New(endpoint, accessKey, secretKey, true)
	if err != nil {
		return nil, err
	}
	if "" == bucket {
		return nil, errors.New("bucket cannot be empty")
	}

	return &MinioObjectStore{
		client: client,
		bucket: bucket,
	}, nil
}

func (mos *MinioObjectStore) SaveObject(obj *ellie.Object) error {

	poo := minio.PutObjectOptions{
		UserMetadata: obj.Metadata,
		ContentType:  obj.Type,
	}
	_, err := mos.client.PutObject(mos.bucket, mos.generatePath(obj.Path), bytes.NewReader(obj.Data), int64(len(obj.Data)), poo)

	return err
}

func (mos *MinioObjectStore) RetrieveObject(path []string) (*ellie.Object, error) {
	obj, err := mos.client.GetObject(mos.bucket, mos.generatePath(path), minio.GetObjectOptions{})
	if err != nil {
		return nil, err
	}
	info, err := obj.Stat()
	if err != nil {
		return nil, err
	}
	data, err := ioutil.ReadAll(obj)
	if err != nil {
		return nil, err
	}

	leinfo := ellie.ObjectInfo{
		Hash:     info.Metadata.Get("hash"),
		Path:     path,
		Type:     info.ContentType,
		Metadata: make(map[string]string),
	}
	for k := range info.Metadata {
		leinfo.Metadata[k] = info.Metadata.Get(k)
	}
	leobj := ellie.Object{
		Data: data,
	}
	leobj.ObjectInfo = leinfo

	return &leobj, nil
}

func (mos *MinioObjectStore) retrieveLatest(path []string) *ellie.ObjectInfo {
	//mos.client.StatObject()
	mos.client.L
	return strings.Join(path, "/")
}

func (mos *MinioObjectStore) generatePath(path []string) string {
	return strings.Join(path, "/")
}
