module gitlab.com/jscarter3/ellie/module/leminio

require (
	github.com/go-ini/ini v1.41.0 // indirect
	github.com/minio/minio-go v6.0.14+incompatible // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	gitlab.com/jscarter3/ellie v0.0.0
	golang.org/x/crypto v0.0.0-20190131182504-b8fe1690c613 // indirect
	golang.org/x/net v0.0.0-20190125091013-d26f9f9a57f3 // indirect
	golang.org/x/sys v0.0.0-20190203050204-7ae0202eb74c // indirect
	golang.org/x/text v0.3.0 // indirect
)

replace gitlab.com/jscarter3/ellie => ../..
