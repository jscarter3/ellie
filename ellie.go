package ellie

import (
	"net/http"
)

type Ellie struct {
	router      *http.ServeMux
	auditStore  AuditStore
	objectStore ObjectStore
	hashCache   HashCache
}

func New(os ObjectStore, as AuditStore, hc HashCache) *Ellie {
	e := &Ellie{
		router:      http.NewServeMux(),
		objectStore: os,
		auditStore:  as,
		hashCache:   hc,
	}
	e.buildRoutes()

	return e
}

func (e *Ellie) Routes() *http.ServeMux {
	return e.router
}

type ObjectStore interface {
	SaveObject(obj *Object) error
	RetrieveObject(path []string) (*Object, error)
	ListVersions(path []string) ([]*ObjectInfo, error)
	// TODO: RetrieveVersion(path []string, version string) (*Object, error)
}

type AuditStore interface {
	SaveAudit(event AuditEvent)
	SaveAccess(event AuditEvent)
	Search()
}

type HashCache interface {
	Check(path string) bool
}

type AuditEvent struct {
	Path []string
	User string
}

type Object struct {
	ObjectInfo
	Data []byte
}

type ObjectInfo struct {
	Path     []string
	Metadata map[string]string
	//Hash      string
	Type      string
	Timestamp int64
}
