package objectstore

import (
	"bufio"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/jscarter3/ellie"
)

type DiskStore struct {
	basePath string
}

func NewDisk(basePath string) *DiskStore {
	return &DiskStore{
		basePath: basePath,
	}
}

func (ds *DiskStore) SaveObject(object *ellie.Object) error {
	fullPath := ds.generatePath(object.Path)
	os.MkdirAll(fullPath, os.ModePerm)
	f, err := os.Create(filepath.Join(fullPath, fmt.Sprintf("%d-%s-%s", time.Now().Unix(), object.Type, object.Hash)))
	if err != nil {
		return err
	}
	defer f.Close()
	w := bufio.NewWriter(f)
	_, err = w.Write(object.Data)
	if err != nil {
		return err
	}
	return w.Flush()
}

func (ds *DiskStore) RetrieveObject(path []string) (*ellie.Object, error) {
	latest := ds.retrieveLatest(path)
	if latest == nil {
		return nil, errors.New("object not found")
	}
	fullPath := ds.generatePath(path)
	data, err := ioutil.ReadFile(filepath.Join(fullPath, fmt.Sprintf("%d-%s-%s", latest.Timestamp, latest.Type, latest.Hash)))
	if err != nil {
		return nil, err
	}
	ret := ellie.Object{
		ObjectInfo: *latest,
		Data:       data,
	}
	return &ret, nil
}

func (ds *DiskStore) retrieveLatest(path []string) *ellie.ObjectInfo {

	var latest *ellie.ObjectInfo
	fullPath := ds.generatePath(path)
	files, err := ioutil.ReadDir(fullPath)
	if err != nil {
		return nil
	}

	for _, file := range files {
		cur := ellie.ObjectInfo{
			Path: path,
		}
		split := strings.Split(file.Name(), "-")
		fmt.Sscanf(split[0], "%d", &cur.Timestamp)
		if latest == nil || cur.Timestamp > latest.Timestamp {
			cur.Type = split[1]
			cur.Hash = split[2]
			latest = &cur

		}
	}
	return latest
}

func (ds *DiskStore) generatePath(path []string) string {
	fullPath := append([]string{ds.basePath}, path...)
	return filepath.Join(fullPath...)
}
