package ellie

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
)

func Test_Post(t *testing.T) {
	res, err := http.Post("http://localhost:9999/api/object/whatever", "application/json", bytes.NewReader([]byte("some data and stuff")))
	if err != nil {
		panic(err)
	}
	b, err := ioutil.ReadAll(res.Body)
	fmt.Println(res.StatusCode)
	fmt.Println(string(b))
}

func Test_Get(t *testing.T) {
	res, err := http.Get("http://localhost:9999/api/object/whatever")
	if err != nil {
		panic(err)
	}
	b, err := ioutil.ReadAll(res.Body)
	fmt.Println(res.StatusCode)
	fmt.Println(string(b))
}
