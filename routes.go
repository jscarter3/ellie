package ellie

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"gitlab.com/jscarter3/ellie/hasher"
)

func (e *Ellie) buildRoutes() {
	e.router.Handle("/api/", e.handler())
}

func (e *Ellie) handler() http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		path := strings.TrimPrefix(r.RequestURI, "/api/")
		split := strings.Split(path, "/")
		switch split[0] {
		case "object":
			e.objectHandler(w, r, split[1:])
		case "track":
			e.trackHandler(w, r, split[1:])
		default:
			fmt.Printf("unknown path: %s\n", path)
		}
	}
	return http.HandlerFunc(fn)
}

func (e *Ellie) trackHandler(w http.ResponseWriter, r *http.Request, path []string) {
	fmt.Printf("%v - %s - track - %v\n", time.Now(), r.Method, path)
}

func (e *Ellie) objectHandler(w http.ResponseWriter, r *http.Request, path []string) {
	switch r.Method {
	case http.MethodPost:
		data, err := ioutil.ReadAll(r.Body)
		if err != nil {
			fmt.Printf("error reading object: %v\n", err)
			return
		}
		r.Body.Close()
		pl := hasher.NewDefault()
		info := ObjectInfo{
			Type:      "default",
			Hash:      pl.Hash(data),
			Timestamp: time.Now().Unix(),
			Path:      path,
		}
		obj := Object{
			ObjectInfo: info,
			Data:       data,
		}
		err = e.objectStore.SaveObject(&obj)
		if err != nil {
			fmt.Printf("error saving object: %v\n", err)
			return
		}
	case http.MethodGet:
		obj, err := e.objectStore.RetrieveObject(path)
		if err != nil {
			fmt.Printf("error loading object: %v\n", err)
			return
		}
		_, err = w.Write(obj.Data)
		if err != nil {
			fmt.Printf("error returning object: %v\n", err)
			return
		}
	default:
		fmt.Printf("unsupported method on object handler: %s\n", r.Method)
	}
	fmt.Printf("%v - %s - object - %v\n", time.Now(), r.Method, path)
}
