# Ellie

Ellie is a framework for building an audit/access tracking service. It provides a set of common interfaces to store
data, track access, and calculate differences between versions. Multiple underlying implementations are planned for
object and metadata storage, along with specific support for various data formats.