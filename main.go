package ellie

func main() {
	//
	//os := objectstore.NewDisk("test_os/")
	//as := mockAuditStore{}
	//
	//e := New(os, &as)
	//
	//s := &http.Server{
	//	Addr:           ":9999",
	//	Handler:        e.Routes(),
	//	ReadTimeout:    10 * time.Second,
	//	WriteTimeout:   10 * time.Second,
	//	MaxHeaderBytes: 1 << 20,
	//}
	//log.Fatal(s.ListenAndServe())
	//
	//fmt.Println("boop")
}

type mockAuditStore struct {
}

func (mas *mockAuditStore) SaveAudit(event AuditEvent) {
	panic("implement me")
}

func (mas *mockAuditStore) SaveAccess(event AuditEvent) {
	panic("implement me")
}

func (mas *mockAuditStore) Search() {
	panic("implement me")
}
