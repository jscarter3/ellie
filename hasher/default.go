package hasher

import (
	"crypto/md5"
	"encoding/hex"
)

type Default struct {
}

func NewDefault() *Default {
	return &Default{}
}

func (pl *Default) Hash(data []byte) string {
	h := md5.New()
	h.Write(data)
	return hex.EncodeToString(h.Sum(nil))
}
