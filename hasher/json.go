package hasher

type JSON struct {
	IgnoredPaths []string
}

func NewJSON(ignoredFields []string) *JSON {
	return &JSON{
		IgnoredPaths: ignoredFields,
	}
}

func (j *JSON) Hash(data []byte) string {
	return ""
}
