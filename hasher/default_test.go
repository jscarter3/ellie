package hasher

import (
	"testing"
)

func TestDefault_Hash(t *testing.T) {
	data := []byte("hello")
	d := NewDefault()

	hash := d.Hash(data)
	if hash != "5d41402abc4b2a76b9719d911017c592" {
		t.Fail()
	}
}
